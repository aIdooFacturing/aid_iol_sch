package com.unomic.dulink.mtc.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/mtc")
@Controller
public class MtcController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MtcController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
//	@Autowired
//	private DeviceService deviceService;
	
	@RequestMapping(value="demo")
	public String demo(){
		return "main/demo";
	};
	
	@RequestMapping(value = "demoBody")
	@ResponseBody
	public String demoBody(HttpServletRequest request){
		
		return "demoBody";
	}

}
