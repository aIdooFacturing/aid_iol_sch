package com.unomic.dulink.scheduler.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonFunction;
//import com.unomic.dulink.device.domain.DeviceVo;
import com.unomic.dulink.iol.service.IolService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/scheduler")
@Controller
public class SchedulerController {

	private static final Logger logger = LoggerFactory.getLogger(SchedulerController.class);
	
	@Autowired
	private IolService iolService;

	
	//@Scheduled(fixedDelay = 10000)
	public void runPolling(){
		getIOLdata();
	}
	
	@RequestMapping(value="testPolling")
	@ResponseBody
	public String testPolling(){

		getIOLdata();
		return "OK";
	}
	
	
	
	
	@RequestMapping(value="getVmType")
	@ResponseBody
	public String getVmType(){
		String s = System.getProperty("java.vm.name");
		
		return s;
	}
	

	public void getIOLdata(){
		//iolService.getIOLData();
	}

}

