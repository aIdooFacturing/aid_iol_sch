package com.unomic.dulink.common.domain;

import java.util.HashMap;
import java.util.Map;

public class CommonCode {
	//public static String myApiKey = "AIzaSyA0hVpizUS0REO6Xxu0EhYqNR8owmkVBww";
	//public static String myApiKey = "AIzaSyA2X_BiUXtFdM-nbCPgcPq88kbzagmP50Q";
	//public static String myApiKey = "AIzaSyDVXoY34Mnj9YB9g5RBQ6scwd6v_X1jShg";
	public static String UTC_ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	public static String DATE_TIME_FORMAT_SEC = "yyyy-MM-dd HH:mm:ss";
	public static String DATE_FORMAT = "yyyy-MM-dd";
	
	public static final String MSG_FAIL						= "FAIL";
	public static final String MSG_IOL_KEY						= "AID-IOL";
	

	public static String myApiKey 							= "AIzaSyBeRxeV7CK4AibPFMTVGmBDd9XTwB3jc5Q";
	public static final String SERVER_IP 					= "106.240.234.114:8080";
	public static final String POST_URL 					= "/DULink/nfc/adapterPost.do";
	public static final String FILE_EXTEND 					= ".txt";
	
	//public static final String TIME_ZONE					= "Europe/Rome";
	//public static final String TIME_ZONE					= "Asia/Seoul";
	public static final String TIME_ZONE					= "Asia/Shanghai";
	public static final String UTC_TIME_ZONE				= "UTC";
	
	//TARGET = "client"  is CNC Agent.
	//TARGET = "server" is UNOMIC Agent.
	public static final String TARGET 						= "server";
	
	
	// security cd
	public static String CODE_DVC_IOS						= "01300001";
	public static String CODE_DVC_ANDROID					= "01300002";
	
	public static String CODE_GCM_SUCCESS					= "01400001";
	public static String CODE_GCM_FAIL						= "01400002";
	//public static String CODE_GCM_FAIL_EMPTY_USER			= "01400002";
	//public static String CODE_GCM_FAIL_UNREG_USER			= "01400003";
	
	public static String MSG_IN_CYCLE						= "IN-CYCLE";
	public static String MSG_WAIT					  		= "WAIT";
	public static String MSG_ALARM							= "ALARM";
	public static String MSG_NO_CONNECTION				 	= "NO-CONNECTION";
	
	public static String MSG_DATE_START						= "DATE_START";
	public static String MSG_STAT_INIT						= "STAT_INIT";

	public static String MSG_HOUR_STD						= "20";
	public static String MSG_MIN_STD						= "30";
	public static String MSG_SDF_STD						= DATE_FORMAT+" "+MSG_HOUR_STD+":"+MSG_MIN_STD+":00.000";
	public static String MSG_UNAVAIL						= "UNAVAILABLE";
	public static String MSG_XML_DELIMITER					= "</MTConnectStreams>";
	
	public static String MSG_MC_TYPE_RAIL					= "rail";
	public static String MSG_MC_TYPE_BLOCK					= "block";
	public static String MSG_RTN_ERROR_PARSE_FAILED			= "Data Parsing Failed";
	
	public static String MSG_PRGM_TYPE_MAIN					= "main";
	public static String MSG_PRGM_TYPE_PALLET_CHAGE			= "plt_chg";
	public static String MSG_PRGM_END_ON					= "ON";
	public static String MSG_PRGM_END_OFF					= "OFF";
	
	
	public static Integer CONNECT_TIMEOUT					= 3000;
	public static Integer READ_TIMEOUT						= 3000;
	public static Integer IOL_STATUS_LENGTH					= 4;
	
	public static String MSG_ADT_STATUS_SEND_REQ			= "SEND_REQ";
	public static String MSG_ADT_STATUS_NO_REQ				= "NO_REQ";
	
	public static String MSG_HTTP							= "http://";
	public static String MSG_AGT_URL						= "/current";
	public static String MSG_IOL_URL						= "/getParam.cgi?DIStatus_00=?&DIStatus_01=?&DIStatus_02=?&DIStatus_03=?";
	
	public static String MSG_IOL_IP_2							= "10.33.74.232";
	public static String MSG_IOL_IP_3							= "10.33.74.235";
	public static String MSG_IOL_IP_4							= "10.33.74.234";
	public static String MSG_IOL_IP_5							= "10.33.74.233";
	public static String MSG_IOL_IP_9							= "10.33.74.236";
	public static String MSG_IOL_IP_10							= "10.33.74.237";
	public static String MSG_IOL_IP_18							= "10.33.74.233";
	public static String MSG_IOL_IP_19							= "10.33.74.234";
	public static String MSG_IOL_IP_28							= "10.33.74.222";
	public static String MSG_IOL_IP_43							= "10.33.74.212";
	public static String MSG_IOL_IP_31							= "10.33.74.211";
	public static String MSG_IOL_IP_32							= "10.33.74.220";
	public static String MSG_IOL_IP_33							= "10.33.74.221";
	public static String MSG_IOL_IP_34							= "10.33.74.216";
	public static String MSG_IOL_IP_35							= "10.33.74.217";
	public static String MSG_IOL_IP_40							= "10.33.74.218";
	public static String MSG_IOL_IP_29							= "10.33.74.230";
	
	public static String MSG_IOL_ID_2							= "2";
	public static String MSG_IOL_ID_3							= "3";
	public static String MSG_IOL_ID_4							= "4";
	public static String MSG_IOL_ID_5							= "5";
	public static String MSG_IOL_ID_9							= "9";
	public static String MSG_IOL_ID_10							= "10";
	public static String MSG_IOL_ID_18							= "18";
	public static String MSG_IOL_ID_19							= "19";
	public static String MSG_IOL_ID_28							= "28";
	public static String MSG_IOL_ID_43							= "43";
	public static String MSG_IOL_ID_31							= "31";
	public static String MSG_IOL_ID_32							= "32";
	public static String MSG_IOL_ID_33							= "33";
	public static String MSG_IOL_ID_34							= "34";
	public static String MSG_IOL_ID_35							= "35";
	public static String MSG_IOL_ID_40							= "40";
	public static String MSG_IOL_ID_29							= "29";
	
	public static final Map<String , String> MAP_MSG_IOL_IP = new HashMap<String , String>() {{
	    put(MSG_IOL_ID_2,    MSG_IOL_IP_2);
	    put(MSG_IOL_ID_3,    MSG_IOL_IP_3);
	    put(MSG_IOL_ID_4,    MSG_IOL_IP_4);
	    put(MSG_IOL_ID_5,    MSG_IOL_IP_5);
	    put(MSG_IOL_ID_9,    MSG_IOL_IP_9);
	    put(MSG_IOL_ID_10,    MSG_IOL_IP_10);
	    put(MSG_IOL_ID_18,    MSG_IOL_IP_18);
	    put(MSG_IOL_ID_19,    MSG_IOL_IP_19);
	    put(MSG_IOL_ID_28,    MSG_IOL_IP_28);
	    put(MSG_IOL_ID_43,    MSG_IOL_IP_43);
	    put(MSG_IOL_ID_31,    MSG_IOL_IP_31);
	    put(MSG_IOL_ID_32,    MSG_IOL_IP_32);
	    put(MSG_IOL_ID_33,    MSG_IOL_IP_33);
	    put(MSG_IOL_ID_34,    MSG_IOL_IP_34);
	    put(MSG_IOL_ID_35,    MSG_IOL_IP_35);
	    put(MSG_IOL_ID_40,    MSG_IOL_IP_40);
	    put(MSG_IOL_ID_29,    MSG_IOL_IP_29);
	}};
 
	
}

