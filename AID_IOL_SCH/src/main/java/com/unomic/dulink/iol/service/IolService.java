package com.unomic.dulink.iol.service;

import java.util.List;

import com.unomic.dulink.iol.domain.IolVo;

public interface IolService {
	public String inputIolData(IolVo inputVo);
	public String addData(IolVo inputVo);
	public List<IolVo> getListIol();
	public void parseHTML(String url,IolVo inputVo) ;
	public String addListIolData(List<IolVo> listIol);
}
