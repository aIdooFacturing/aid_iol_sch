package com.unomic.dulink.iol.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IolVo{
	int dvcId;
	String startDateTime;
	String endDateTime;
	String status;
	String chartStatus;
	int iolCycleEnd;
	int iolCycleStart;
	String ip;
	String certKey;
	String workDate;
	
}
