package com.unomic.dulink.iol.service;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.iol.controller.IolController;
import com.unomic.dulink.iol.domain.IolVo;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.Source;

@Service
@Repository
public class IolServiceImpl implements IolService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IolController.class);

	public static String IOL_SPACE = "com.unomic.iol.";
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
	@Override
	@Transactional
	public String addData(IolVo inputVo)
	{
		
		sql_ma.insert(IOL_SPACE + "addIolData", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional
	public String addListIolData(List<IolVo> listIol)
	{
		HashMap <String, List<IolVo>> dataMap = new HashMap<String, List<IolVo>>();

		dataMap.put("listIol", listIol);
		sql_ma.insert(IOL_SPACE + "addListIolData", dataMap);
		
		return "OK";
	}
	
	
	
	@Override
	@Transactional
	public String inputIolData(IolVo inputVo)
	{
		
		LOGGER.info("inputVo.getDvcId():"+inputVo.getDvcId());
		int cntKey = (int) sql_ma.selectOne(IOL_SPACE + "cntCertKey", inputVo);
		if(cntKey < 1){
			return "INVALID_KEY";
		}
		
		int cntData = (int) sql_ma.selectOne(IOL_SPACE + "cntAdapterStatus", inputVo);
		if(cntData < 1){ // 이전 데이터 없는 경우
			// addIOLStatus(inputVo);
			sql_ma.insert(IOL_SPACE + "addIolData", inputVo);
		}else{
			//getLastData
			//dateStarter
			//editLastEndTime
			//addIOLStatus
			IolVo preIolVo = (IolVo)sql_ma.selectOne(IOL_SPACE + "getLastIolData", inputVo);
			IolVo starterVo = chkDateStarterIOL(preIolVo, inputVo);

			IolVo lastVo = new IolVo();
			lastVo.setDvcId(inputVo.getDvcId());
			lastVo.setEndDateTime(inputVo.getStartDateTime());
			//데이트 스타터 동작할 경우...
			if(null != starterVo){
				lastVo.setStartDateTime(starterVo.getStartDateTime());
			}else{
				lastVo.setStartDateTime(preIolVo.getStartDateTime());
			}
			sql_ma.update(IOL_SPACE + "editLastEndTime", lastVo);
			sql_ma.insert(IOL_SPACE + "addIolData", inputVo);
		}
		
		return "OK";
	}
	
	public IolVo chkDateStarterIOL(IolVo preVo, IolVo crtVo){

		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			LOGGER.error("@@@@@RUN DateStarter IOL@@@@@");
			LOGGER.error("startDateTime:"+preVo.getStartDateTime());
			LOGGER.error("endDateTime:"+crtVo.getStartDateTime());
			
			IolVo starterVo = new IolVo();
			starterVo.setDvcId(crtVo.getDvcId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setEndDateTime(CommonFunction.getStandardP1SecToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));
			
			IolVo lastVo = new IolVo();
			lastVo.setDvcId(crtVo.getDvcId());
			lastVo.setStartDateTime(preVo.getStartDateTime());
			lastVo.setEndDateTime(crtVo.getStartDateTime());

			sql_ma.update(IOL_SPACE + "editLastEndTime", lastVo);
			sql_ma.insert(IOL_SPACE + "addIolData", starterVo);

			return starterVo;
		}
		return null;
	}
	
	@Override
	public List<IolVo> getListIol()
	{
		List<IolVo> rtnList = (List<IolVo>)sql_ma.selectList(IOL_SPACE + "getListIolDvc");
		
		return rtnList;
	}
	
//	@Override
//	public HashMap<String,String> testParse()
//	{
//		String url2 = "http://106.240.234.116:8080/MOXA/01.htm";
//		String url1 = "http://106.240.234.114:8080/MOXA/01.htm";
//		
//		ArrayList<String> listUrl = new ArrayList<String>();
//		listUrl.add(url1);
//		listUrl.add(url2);
//		
//		
//		HashMap<String,String> mapRtn = new HashMap<String,String>();
//		LOGGER.info("SIZE:"+listUrl.size());
//		Iterator<String> iteUrl = listUrl.iterator();
//		
//		LOGGER.info("idx0 : " + listUrl.get(0));
//		LOGGER.info("idx1 : " + listUrl.get(1));
//		
//		long startTime = System.currentTimeMillis();
//		if(iteUrl.hasNext()){
//			LOGGER.info("RUN!");
//			String tmpUrl = iteUrl.next();
//			LOGGER.info("URL:"+tmpUrl);
//			mapRtn.put(parseHTML(tmpUrl),""+( System.currentTimeMillis() - startTime )/1000.0f);
//			LOGGER.info("endIte");
//		}
//		
//		return mapRtn;
//	}
	
//	@Override
//	public void getIOLData()
//	{
//		SqlSession sql = getSqlSession();
//		
//		List<IolVo> listAdt = getListIOL();
//		int size = listAdt.size();
//		logger.info("number of IOL:"+size);
//		IolVo tmpVo = new IolVo();
//		for(int i = 0 ; i<size ;i++){
//			try {
//				tmpVo = listAdt.get(i);
//				
//				URL url = new URL(CommonCode.MSG_HTTP + tmpVo.getAdtIp() + CommonCode.MSG_IOL_URL);
//				URLConnection con = url.openConnection();
//				con.setConnectTimeout(CommonCode.CONNECT_TIMEOUT);
//				con.setReadTimeout(CommonCode.READ_TIMEOUT);
//				InputStream in = con.getInputStream();
//				
//				BufferedReader br = new BufferedReader(new InputStreamReader(in));
//				String strTemp = org.apache.commons.io.IOUtils.toString(br);
//				IolVo pureVo = getIOLStatus(strTemp,tmpVo.getAdtId());
//
//				DeviceVo setVo = new DeviceVo();
//				setVo.setAdtId(tmpVo.getAdtId());
//				setVo.setLastUpdateTime(pureVo.getStartDateTime());
//				setVo.setLastChartStatus(pureVo.getChartStatus());
//				setVo.setLastStartDateTime(pureVo.getStartDateTime());
//				//setVo.setWorkDate(CommonFunction.getWorkDate(System.currentTimeMillis()));
//				
//				editLastStatusIOL(setVo);
//				
//				IolVo preIOLVo = getLastInputData(pureVo);
//				preIOLVo = chkIOLStatus(preIOLVo);
//				
//				IolVo startVo = chkDateStarterIOL(preIOLVo, pureVo);
//				
//				//logger.info("Before Duple:"+preIOLVo.getEndDateTime());
//				if(null==isDupleIOL(preIOLVo, pureVo)
//						&& null == preIOLVo.getEndDateTime()
//					){//check Duplication.
//					//logger.info("RUN nothing");
//				}else{
//					if(null != startVo){
//						pureVo.setStartDateTime(startVo.getEndDateTime());;
//					}
//					
//					editLastEndTime(pureVo);
//					addIOLStatus(pureVo);
//				}
//				
//			} catch (SocketTimeoutException ex) {
//				logger.error("[ADT_ID:"+tmpVo.getAdtId()+"@@]"+"IOL NO-CONNECTION:"+"[IP:"+ tmpVo.getAdtIp()+"]");
//			}catch (UnknownHostException exx){
//				logger.error("[ADT_ID:"+tmpVo.getAdtId()+"@@]"+"UnknownHostException Error");
//			} catch (Exception e){
//				logger.error("[ADT_ID:"+tmpVo.getAdtId()+"@@]"+"IOL Error");
//				e.printStackTrace();
//			} 
//		}
//		
//	}
	@SuppressWarnings("finally")
	@Override
	public void parseHTML(String url,IolVo inputVo) {
		long startTime = System.currentTimeMillis();
	    StringBuilder sb = new StringBuilder();
	    String ret = "";
	    String rtnStr="";
	    try {
	    	//LOGGER.info("URL:"+url);
	        Source source = new Source(new URL(url));
	        //LOGGER.info("source : " + source.toString());
	        source.fullSequentialParse();
	        String content;
	       
	        List<Element> elementList = source.getAllElements();

	        for(int i = 0; i < elementList.size(); i++) {
	            content = elementList.get(i).getContent().toString();
	            if("ON".equals(content)) {
	                sb.append("1");
	            }
	            else if("OFF".equals(content)) {
	                sb.append("0");
	            }
	        }
	        ret = sb.substring(0, 6);
	        LOGGER.info("StringBuilder : " + sb.substring(0, 6));
	        LOGGER.info("RunTime:"+(System.currentTimeMillis() - startTime )/1000.0f);
	    } catch (FileNotFoundException e1) {
	    	LOGGER.info("File Not Found.");
	    	ret =  "FAIL";
	    } catch (Exception e) {
	        //e.printStackTrace();
	    	LOGGER.info(e.getMessage());
	    	ret =  "FAIL";
	    } finally {
	    	//LOGGER.info("RunTime:"+(System.currentTimeMillis() - startTime )/1000.0f);
	    	if(ret.equals("FAIL")){
	    		LOGGER.error("FAIL");
	    	}else{
	    		inputVo.setStatus(ret);
	    		inputVo = setIolData(inputVo);
	    		rtnStr = inputIolData(inputVo);
	    	}
	        //return rtnStr;
	    }
	}
	
	private String chartStatusChecker(String status){
		
		String rtn = CommonCode.MSG_IN_CYCLE;
		if(status.charAt(2)=='1'){
			rtn = CommonCode.MSG_ALARM;
		}else if(status.charAt(3)=='1'){
			rtn = CommonCode.MSG_WAIT;
		}else {//if(status.charAt(1)=='1'){
			rtn = CommonCode.MSG_IN_CYCLE;
		}
		return rtn;
	}
	
	
	private IolVo setIolData(IolVo setVo){
		
		IolVo addVo = new IolVo();
		addVo.setDvcId(setVo.getDvcId());
		addVo.setCertKey(CommonCode.MSG_IOL_KEY);
		String tmpStatus = setVo.getStatus();
		addVo.setStatus(tmpStatus);
		addVo.setChartStatus(chartStatusChecker(tmpStatus));
		addVo.setIolCycleEnd(Character.getNumericValue(tmpStatus.charAt(4)));
		addVo.setIolCycleEnd(Character.getNumericValue(tmpStatus.charAt(5)));
		addVo.setStartDateTime(CommonFunction.getTodayDateTime());
		addVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
		
		return addVo;
	}
	
	

}
