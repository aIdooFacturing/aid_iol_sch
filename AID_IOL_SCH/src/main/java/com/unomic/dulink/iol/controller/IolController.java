package com.unomic.dulink.iol.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.reflection.ReflectionException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.iol.domain.IolVo;
import com.unomic.dulink.iol.service.IolService;

/**
 * Handles requests for the application home page.
 */

@RequestMapping(value = "/IOL")
@Controller
public class IolController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IolController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	

	
	@Autowired
	private IolService iolService;
	
	
	@RequestMapping(value="viewData")
	public String demo(){
		
		return "IOL/tableBoard";
	};
	
	@RequestMapping(value="testParse")
	@ResponseBody
	public String testParse(){
		
		//HashMap<String,String> rtnStr = iolService.testParse();
//		Set<String>rtnKey = rtnStr.keySet();
//		Iterator<String> iteRtn = rtnStr.values().iterator();
		//Iterator<String> iteKey = rtnKey.iterator();
		String dmn = "http://106.240.234.";
		String fileNm = "/01.htm";
		String url1 = "http://106.240.234.114/01.htm";
		String url2 = "http://106.240.234.116/01.htm";
		
		ArrayList<String> listUrl = new ArrayList<String>();
		listUrl.add(url1);
		listUrl.add(url2);
		
		HashMap<String,String> mapRtn = new HashMap<String,String>();
		LOGGER.info("SIZE:"+listUrl.size());
		
//		LOGGER.info("idx0 : " + listUrl.get(0));
//		LOGGER.info("idx1 : " + listUrl.get(1));
		
//		if(iteUrl.hasNext()){
//			LOGGER.info("RUN!");
//			LOGGER.info("has next?:"+iteUrl.hasNext());
//			String tmpUrl = iteUrl.next();
//			LOGGER.info("URL:"+tmpUrl);
//			//String tmpRtm = "";
//			iolService.parseHTML(tmpUrl);
//			LOGGER.info("has next?:"+iteUrl.hasNext());
//			LOGGER.info("endIte");
//		}
		
		for(String temp : listUrl){
			LOGGER.info("For loop temp:"+temp);
			//iolService.parseHTML(temp);
		}
		
		Long startTime = System.currentTimeMillis();
//		iolService.parseHTML(url1);
//		iolService.parseHTML(url2);
		
		LOGGER.info("RUNTM:"+ (System.currentTimeMillis() - startTime )/1000.0f);
//		if(iteKey.hasNext()){
//			LOGGER.info(""+iteKey.next());
//		}
//		
//		if(iteRtn.hasNext()){
//			LOGGER.info(""+iteRtn.next());
//		}
		
		return "OK";
	};
	
	@RequestMapping(value="getIolData")
	@ResponseBody
	public String getIolData(){
		Long startTime = System.currentTimeMillis();

		String prtc = "http://";
		String fileNm = "/01.htm";
		String rtnSignal = CommonCode.MSG_FAIL;
		
		List<IolVo> listIol = (List<IolVo>) iolService.getListIol();
		ArrayList<String> listUrl = new ArrayList<String>();
		
		for(int i=0;i<listIol.size();i++){
			IolVo tmpVo = listIol.get(i);
			//listUrl.add(prtc+tmpVo.getIp()+fileNm);
			String tmpUrl = prtc+tmpVo.getIp()+fileNm;
			LOGGER.info("tmpVo.getDvcId : "+tmpVo.getDvcId());
			LOGGER.info("tmpUrl : "+tmpUrl);
			iolService.parseHTML(tmpUrl,tmpVo);
		}
		
		//String rtnStr = batchAdd(listIol);

		LOGGER.info("RUNTM2:"+ (System.currentTimeMillis() - startTime )/1000.0f);
		
		return "OK";
	};
	
	
	@Scheduled(fixedDelay = 10000)
	public void schIolData(){
		Long startTime = System.currentTimeMillis();

		String prtc = "http://";
		String fileNm = "/01.htm";
		String rtnSignal = CommonCode.MSG_FAIL;
		
		List<IolVo> listIol = (List<IolVo>) iolService.getListIol();
		ArrayList<String> listUrl = new ArrayList<String>();
		
		for(int i=0;i<listIol.size();i++){
			IolVo tmpVo = listIol.get(i);
			//listUrl.add(prtc+tmpVo.getIp()+fileNm);
			String tmpUrl = prtc+tmpVo.getIp()+fileNm;
			LOGGER.info("tmpVo.getDvcId : "+tmpVo.getDvcId());
			LOGGER.info("tmpUrl : "+tmpUrl);
			iolService.parseHTML(tmpUrl,tmpVo);
		}
		//String rtnStr = batchAdd(listIol);
		LOGGER.info("RUNTM2:"+ (System.currentTimeMillis() - startTime )/1000.0f);
	};
	
	//리스트 가져와서 차례대로 데이터 가져옴.
	//장비별 서비스로 호출해서 이전장비에서 데이터 못가져와도 지연없이 거의 동시에 끝나도록.
	//순서.
	// 1 장비 리스트 가져옴
	// 2 id, ip별 요청.
	// 리턴값 모음.
	// 쿼리 자원 절약을 위해 한번에 입력.
	// 비동기 처리 연구 필요.;;;
	@RequestMapping(value="getListIOL")
	@ResponseBody
	public List<IolVo> getListIOL(){
		List<IolVo> rtnList = (List<IolVo>) iolService.getListIol();
		
//		for(AmcVo i :rtnList){
//			LOGGER.info(i.toString());
//		}
		
		return rtnList;
	};
	

	public String dtFMChecker(String datetime){

		DateFormat sdf = new SimpleDateFormat(CommonCode.DATE_TIME_FORMAT_SEC);
		sdf.setTimeZone(TimeZone.getTimeZone(CommonCode.TIME_ZONE));
		Date date = new Date(System.currentTimeMillis());
		try {
			date= sdf.parse(datetime);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOGGER.error(e.getErrorOffset()+"DT_ParseException");

			return null; 
		}
		return date.toString();
	}
	
	public String batchAdd(List<IolVo> listIol){
		String rtnCode =CommonCode.MSG_FAIL;
		try {
			rtnCode = iolService.addListIolData(listIol);
		} catch (ReflectionException e) {
			LOGGER.error("ReflectionException");
			rtnCode = "DUPLE";
		}
		
		switch (rtnCode){
		case "OK":
			return rtnCode;
		default:
			return "FAIL : "+rtnCode;
		}
	}
	
	
}