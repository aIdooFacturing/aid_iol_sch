<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% String cp = request.getContextPath(); %> <%--ContextPath 선언 --%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<%=cp%>/resources/images/favicon.ico">

    <title>aIdoo seobong test page</title>

    <!-- Bootstrap core CSS -->
    <link href="<%=cp%>/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<%=cp%>/resources/IOL/tableBoard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Check Input Data</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Overview <span class="sr-only">(current)</span></a></li>
          </ul>

        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">TestPage</h1>

          <h2 class="sub-header">Section title</h2>
          
          <input type="button" value="refreash" onclick="getList();"/>
          <div class="table-responsive" >
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>DVC_ID</th>
                  <th>START_DATE_TIME</th>
                  <th>STATUS</th>
                  <th>CHART_STATUS</th>
                  <th>CYCLE_END</th>
                </tr>
              </thead>
              <tbody id='testTbody'>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="<%=cp%>/resources/bootstrap/js/bootstrap.min.js"></script>
    <script>
    //화면 초기 실행 
	jQuery(document).ready(function() {
		//for Popup Notice;
		getList();
	});
    
    function getList(){
    	var url="<%=cp%>/IOL/getListIOL.do";
    	$.ajax({
    		url : url,
    		/* data : param, */
    		dataType : "json",
    		type : "post",
    		success : function(listJson){
    			$('#testTbody').html('');
    			listJson.forEach( function( v, i ){
    				  /* console.log(v); */
    				    var tmp="<tr>\
    				      <td>"+v.dvcId+"</td>\
    				      <td>"+v.startDateTime+"</td>\
    				      <td>"+v.status+"</td>\
    				      <td>"+v.chartStatus+"</td>\
    				      <td>"+v.iolCycleEnd+"</td>\
    	                </tr>"
    				      $('#testTbody').append(tmp);
    				    
				});
    		}
    	});
    }
	</script> 
       
  </body>
</html>
